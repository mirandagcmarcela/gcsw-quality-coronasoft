package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalendarioPage {

	
	
	WebDriver driver;

	//@FindBy(xpath = ".//h3")
	@FindBy(xpath = ".//h2")
	private WebElement mesActual;    
    
    public CalendarioPage(WebDriver driver){
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    }
	    
    public boolean EsMensajeMostrado(String un) {
    	return mesActual.getAttribute("textContent").equals(un);
    } 


}
