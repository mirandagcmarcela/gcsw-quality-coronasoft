package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.HomeSPage;
import Pages.CalendarioPage;

public class VerificarMesActualCalendarioTest {

	String baseUrl = "http://llajtatours.ddns.net/#/";
	public WebDriver driver;
	private HomeSPage homePage;
	private CalendarioPage calendarioPage;
	
	@Test
	public void VerificarQueSeMuestraUnCalendarioCuandoSeIngresaAlaPestanaCalendarioDeFestividades() {
	    
		homePage = new HomeSPage(driver);
		calendarioPage = new CalendarioPage(driver);
		homePage.SeleccionarCalendario();
	    Assert.assertTrue(calendarioPage.EsMensajeMostrado("junio de 2020"), "Message was not displayed");
	  }
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");					
		driver = new ChromeDriver();		
		//driver = new FirefoxDriver();				  									
		driver.get(baseUrl);
	}
	
    @AfterTest
    public void quit() {   	
    	//driver.close();	
    }
    

}
