package Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.Functions;

public class VerifyThatSearchingWithoutPlacingAnyCharactersShowsAllResults {
	
	String baseUrl = "http://llajtatours.ddns.net/#/";
	public WebDriver driver;
	private Functions function;
	
	//private LoginSuccessPage SucessPage;
	
	By registerLink = By.linkText("REGISTER");
	By Flights = By.linkText("Flights");

	
	
	@Test
	public void VerifyThatSearchingWithoutPlacingAnyCharactersShowsAllResults_(){
		function = new Functions(driver );
		function.selectDropReat();
		
	  }
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");					
		driver = new ChromeDriver();					  									
		driver.get(baseUrl);
	}
	
    @AfterTest
    public void quit() {   	
    	driver.close();	
    }
    

}
