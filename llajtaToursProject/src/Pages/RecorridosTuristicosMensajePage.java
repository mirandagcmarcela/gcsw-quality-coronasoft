package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RecorridosTuristicosMensajePage {
	
	WebDriver driver;

	//@FindBy(xpath = ".//p")
	@FindBy(xpath = ".//p")
	private WebElement mensajeError;    
    
    public RecorridosTuristicosMensajePage(WebDriver driver){
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    }
	    
    public boolean EsMensajeMostrado(String un) {
    	System.out.println(mensajeError.getAttribute("textContent"));
    	return mensajeError.getAttribute("textContent").equals(un);
    } 

}
