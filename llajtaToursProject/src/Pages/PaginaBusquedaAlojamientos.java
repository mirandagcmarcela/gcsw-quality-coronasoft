package Pages;

import javax.lang.model.element.Element;
import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaBusquedaAlojamientos {

	WebDriver driver;

	@FindBy(xpath = ".//div[contains(text(), 'Buscar Servicios')]")
	private WebElement dropp;
	
	@FindBy(xpath = ".//span[contains(text(),'B�squeda por Alojamiento')]")
	private WebElement enlace;
	
	@FindBy(xpath = "//button[@class='ui button']")
	private WebElement boton;
	
	By input = By.xpath("//input[@placeholder='Escriba algo para buscar']");

	public PaginaBusquedaAlojamientos(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void EnterCredentials(String dato) {
		dropp.click();
		enlace.click();
		driver.findElement(input).sendKeys(dato);
		boton.click();
	}
}
