package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AventuraPage {

	WebDriver driver;
	
	@FindBy(xpath = ".//input[@placeholder='Escriba algo para buscar']")
	private WebElement barraBusqueda;
	@FindBy(xpath = ".//button[@type='submit']")
	private WebElement botonBuscar;
	
	public AventuraPage(WebDriver driver) {	
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}

	public 	AventuraMensajeErrorPage IngresarDatos(String aventura) {
		barraBusqueda.sendKeys(aventura);
		botonBuscar.click();
		
		return new AventuraMensajeErrorPage(driver);
	}  
}
