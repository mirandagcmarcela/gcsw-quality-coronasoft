package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.AventuraPage;
import Pages.AventuraMensajeErrorPage;

public class VerificarMensajeErrorBuscadorAventuraTest {
	String baseUrl = "http://llajtatours.ddns.net/#/search-adventure";
	public WebDriver driver;
	private AventuraPage aventuraPage;
	private AventuraMensajeErrorPage aventuraMensajeErrorPage;
	
	@Test
	public void VerificarQueSeMuestraUnMensajeDeErrorAlIngresarDatosErroneos() {
	    
		aventuraPage = new AventuraPage(driver);
		aventuraMensajeErrorPage = new AventuraMensajeErrorPage(driver);
		aventuraPage.IngresarDatos("gciclist");
	    Assert.assertFalse(aventuraMensajeErrorPage.EsMensajeMostrado("No se ha podido encontrar la aventura, ingrese datos nuevamente"), "Message was not displayed");
	  }
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");					
		driver = new ChromeDriver();		
		//driver = new FirefoxDriver();				  									
		driver.get(baseUrl);
	}
	
    @AfterTest
    public void quit() {   	
    	driver.close();	
    }
    

}
