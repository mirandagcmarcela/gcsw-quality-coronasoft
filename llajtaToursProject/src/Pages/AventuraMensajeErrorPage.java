package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AventuraMensajeErrorPage {
	
	WebDriver driver;

	//@FindBy(xpath = ".//h3")
	@FindBy(xpath = ".//p")
	private WebElement mensajeError;    
    
    public AventuraMensajeErrorPage(WebDriver driver){
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    }
	    
    public boolean EsMensajeMostrado(String un) {
    	return mensajeError.getAttribute("textContent").equals(un);
    } 

}
