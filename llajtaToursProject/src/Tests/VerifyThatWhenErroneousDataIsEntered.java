package Tests;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Pages.Home;


public class VerifyThatWhenErroneousDataIsEntered {


	 String baseUrl = "http://llajtatours.ddns.net/#/search-accommodation";
		public WebDriver driver;
		private Home home;
		
		
		@Test
		public void Verify() {
		    
			home = new Home(driver);
			home.selectSearch();
			Assert.assertFalse(home.IsMessageDisplayedv2("No se ha podido encontrar alojamientos, ingrese datos nuevamente"), "Message was not displayed");
		}
		
		@BeforeTest
		public void setup() {
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");				
			driver = new ChromeDriver();					  									
			driver.get(baseUrl);
			
			
		}
		
	    @AfterTest
	    public void quit() {   	
	    	driver.close();	
	    }
	    

}