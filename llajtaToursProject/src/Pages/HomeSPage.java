package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomeSPage {

	WebDriver driver;
	

	@FindBy(xpath = ".//*[a='Calendario de festividades']")
	private WebElement calendario;
	
	
	public HomeSPage(WebDriver driver) {	
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}

	public 	CalendarioPage SeleccionarCalendario() {
		
		calendario.click();
		return new CalendarioPage(driver);
	}  


}
