package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Functions{
	
	WebDriver driver;
	
	//WebElements
	//WebElement userName = driver.findElement(By.name("userName"));								
	//WebElement password = driver.findElement(By.name("password"));								
	//WebElement submit = driver.findElement(By.name("submit"));
	
	
	
	//By dropp = By.cssSelector("div[aria-expanded=\"false\"]")
  
	
	@FindBy(xpath = ".//div[contains(@class, 'ui button floating labeled dropdown icon')]")
	private WebElement dropp;
	@FindBy(xpath = ".//span[contains(text(),'Actividades por Recorrido Turístico')]")
	private WebElement url;
	@FindBy(xpath = ".//button[contains(text(),'Buscar')]")
	private WebElement buscar;
	
	@FindBy(xpath = ".//*[@placeholder='Escriba algo para buscar']")
	private WebElement camp;
	
	
    public Functions(WebDriver driver){
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    }
   
    
    
    
    public void selectDropReat() {
		dropp.click();
		url.click();
		buscar.click();
	}
    
    
    
 
    public void maxNumber(String x) {
    	dropp.click();
		url.click();
		camp.clear();
		camp.sendKeys(x);
	}
}
