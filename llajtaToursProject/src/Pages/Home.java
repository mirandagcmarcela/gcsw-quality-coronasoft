package Pages;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home {
	
		WebDriver driver;
		
		
		@FindBy(xpath = ".//p")
	    private WebElement message;
		@FindBy(xpath = "//button[text()='Buscar']")
		private WebElement button;
		@FindBy(xpath = "//input[@placeholder='Escriba algo para buscar']")
		private WebElement search;
		
		
	    public Home(WebDriver driver){
	    	this.driver = driver;
	        PageFactory.initElements(driver, this);
	    }
	    
	    public void selectSearch() {
	    	search.clear();
	    	search.sendKeys("pairumani");
	    	button.click();
	    
	    }  
	    
	    public boolean IsMessageDisplayedv2(String un) {
			
			   return message.getText().equals(un);
		    }

}


