package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaBusquedaAlojamientoSucces {

	WebDriver driver;
	@FindBy(xpath = "//a[@class='header']")
	private WebElement tituloAlojamiento;   
    
    public PaginaBusquedaAlojamientoSucces(WebDriver driver){
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    }
	    
    public boolean existeTitulo(String un) {
    	return tituloAlojamiento.getAttribute("textContent").equals(un);
    } 

}
