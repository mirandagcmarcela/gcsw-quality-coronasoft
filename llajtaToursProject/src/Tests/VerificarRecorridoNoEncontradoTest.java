package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.RecorridosTuristicosMensajePage;
import Pages.RecorridosTuristicosPage;


public class VerificarRecorridoNoEncontradoTest {
	String baseUrl = "http://llajtatours.ddns.net/#/search-tour";
	public WebDriver driver;
	private RecorridosTuristicosPage recorridosPage;
	private RecorridosTuristicosMensajePage recorridosMensajePage;
	
	@Test
	public void VerificarQueSeMuestraUnMensajeDeRecorridoNoEncontrado() {
	    
		recorridosPage = new RecorridosTuristicosPage(driver);
		recorridosMensajePage = new RecorridosTuristicosMensajePage(driver);
		recorridosPage.IngresarDatos("El prado");
	    Assert.assertFalse(recorridosMensajePage.EsMensajeMostrado("No se ha podido encontrar recorridos turísticos, ingrese datos nuevamente"), "Message was not displayed");
	  }
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");					
		driver = new ChromeDriver();					  									
		driver.get(baseUrl);
	}
	
    @AfterTest
    public void quit() {   	
    	driver.close();	
    }
    

}
