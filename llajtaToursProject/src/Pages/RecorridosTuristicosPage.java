package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RecorridosTuristicosPage {

	WebDriver driver;
	
	@FindBy(xpath = ".//input[@placeholder='Escriba algo para buscar']")
	private WebElement barraBusqueda;
	@FindBy(xpath = ".//button[@type='submit']")
	private WebElement botonBuscar;
	
	public RecorridosTuristicosPage(WebDriver driver) {	
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}

	public RecorridosTuristicosMensajePage IngresarDatos(String lugar) {
		barraBusqueda.sendKeys(lugar);
		botonBuscar.click();
		
		return new RecorridosTuristicosMensajePage(driver);
	}  
}
