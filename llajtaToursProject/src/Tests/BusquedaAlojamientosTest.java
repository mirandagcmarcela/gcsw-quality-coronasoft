package Tests;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.PaginaBusquedaAlojamientoSucces;
import Pages.PaginaBusquedaAlojamientos;

public class BusquedaAlojamientosTest {

	String baseUrl = "http://llajtatours.ddns.net/#/";
	public WebDriver driver;
	private PaginaBusquedaAlojamientos page;
	private PaginaBusquedaAlojamientoSucces pagesuccess;
	
	@Test
	public void verificarbusqueda() {
	    
		page = new PaginaBusquedaAlojamientos(driver);
		pagesuccess = new PaginaBusquedaAlojamientoSucces(driver);
		page.EnterCredentials("Hotel Coch");
	    Assert.assertTrue(pagesuccess.existeTitulo("Gran Hotel Cochabamba"), "No se encontro Hotel");
	  }
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32_v83\\chromedriver.exe");					
		driver = new ChromeDriver();					  									
		driver.get(baseUrl);
		
	}
	
    @AfterTest
    public void quit() {   	
    	//driver.close();	
    }
}
